const globalCache = global;

module.exports = function (newman) {
    newman.on('done', () => {
        global = globalCache;
    });
}
