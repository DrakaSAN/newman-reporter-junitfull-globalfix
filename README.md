# newman-reporter-junitfull-globalfix

This package aim to provide a fix for an [issue with newman](https://github.com/postmanlabs/newman/issues/3042) where sequentials calls to `newman.run` while using the reporter `junitfull` is breaking with an error:

```sh
$ node test
Running collection 'DDG' ...

/path/to/project/node_modules/newman/lib/run/secure-fs.js:72
    this.isWindows = global.process.platform === 'win32';
                                    ^

TypeError: Cannot read properties of undefined (reading 'platform')
    at new SecureFS (/path/to/project/node_modules/newman/lib/run/secure-fs.js:72:37)
    at /path/to/project/node_modules/newman/lib/run/index.js:179:27
    at /path/to/project/node_modules/newman/lib/run/options.js:394:9
    at wrapper (/path/to/project/node_modules/async/dist/async.js:271:20)
    at /path/to/project/node_modules/async/dist/async.js:3382:19
    at wrapper (/path/to/project/node_modules/async/dist/async.js:271:20)
    at iterateeCallback (/path/to/project/node_modules/async/dist/async.js:427:28)
    at /path/to/project/node_modules/async/dist/async.js:327:20
    at /path/to/project/node_modules/async/dist/async.js:3380:17
    at done (/path/to/project/node_modules/newman/lib/run/options.js:202:17)
    at processCollection (/path/to/project/node_modules/newman/lib/run/options.js:105:9)
    at /path/to/project/node_modules/newman/lib/run/options.js:220:24
    at /path/to/project/node_modules/newman/lib/run/options.js:82:20
    at /path/to/project/node_modules/newman/lib/util.js:222:24
    at FSReqCallback.readFileAfterClose [as oncomplete] (node:internal/fs/read_file_context:68:3)
```

This bug is not in newman but in the [reporter](https://github.com/martijnvandervlag/newman-reporter-junitfull), which actually have an [open PR](https://github.com/martijnvandervlag/newman-reporter-junitfull/pull/1), but since the project seems unmaintained, it is doubtful it will be fixed.

The bug come from the reporter rewriting the `global` variable.

This reporter will not hange anything in the output but simply rewrite `global` to its original value in the hook after the one used by `junitfull`.

## Usage

Install this reporter and use it along with `junitfull`:

```sh
npm i -D newman-reporter-junitfull-globalfix
newman -r junitfull,junitfull-global-fix
```

Or in your JSON config:

```json
"reporters": ["cli", "junitfull", "junitfull-globalfix"],
```
